﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file is a modification of the original, which has the following copyright:
// Copyright (c) 2018 Michael Curtiss
// The Original, licensed MIT can be found here: https://github.com/brettmjohnson/Outliner




using UnityEngine;
using System.Collections.Generic;
using System.Linq;

using UnityEngine.Rendering;

namespace OutlineFx
{
	[RequireComponent(typeof(Camera))]
	public class OutlineRenderer : MonoBehaviour
	{
		public Color outlineColor = new Color(1, 0, 0, 0.5f);

		[Range(0, 1)]
		public float occludedOutlineOpacity = 0.3f;

		[Range(0, 3)]
		public float blurSize = 1.0f;

		[Range(0, 1)]
		public float alphaCutoff = 0.1f;

		/// <summary>
		/// The event to trigger drawing.
		/// </summary>
		public CameraEvent bufferDrawEvent = CameraEvent.BeforeImageEffects;

		private CommandBuffer commandBuffer;
		private Material outlineMaterial;
		private Camera cam;

		private bool hasComplainedAboutMSAA;

		private HashSet<OutlinedObject> outlinedObjects = new HashSet<OutlinedObject>();

		private int instanceId;

		private string commandBufferName;

		private void Awake()
		{
			instanceId = GetInstanceID(); // method so expensive?

			// We allow multiple components, so we need unique Command Buffer names.
			commandBufferName = $"OutlineRenderer Command Buffer for {name} ({instanceId})";
		}


		private void OnEnable()
		{
			if (outlinedObjects.Count == 0) return;
			CreateCommandBuffer();
		}


		private void OnDisable()
		{
			Cleanup();
		}


		// update if values change at runtime - in editor only
		private void OnValidate()
		{
			if (outlinedObjects.Count == 0) return;
			CreateCommandBuffer();
		}


		private void Cleanup()
		{
			if (commandBuffer == null) return;
			cam.RemoveCommandBuffer(bufferDrawEvent, commandBuffer);
			commandBuffer = null;
		}

		#region Targets

		/// <summary>
		/// Clear the current selection and add the given target.
		/// </summary>
		/// <param name="t"></param>
		public void SetOutlinedObject(Transform t)
		{
			SetOutlinedObject(t.gameObject);
		}

		/// <summary>
		/// Clear the current selection and add the given target.
		/// </summary>
		/// <param name="go"></param>
		public void SetOutlinedObject(GameObject go)
		{
			Clear_Internal();
			AddOutlinedObject(go);
		}

		internal void AddOutlinedObject(OutlinedObject obj)
		{
			if (outlinedObjects.Add(obj))
				CreateCommandBuffer();
		}

		/// <summary>
		/// Add an object to outline to this <see cref="OutlineRenderer"/>
		/// </summary>
		public void AddOutlinedObject(GameObject go)
		{
			var outlinedBehaviour = GetLinkedOrAddNewOutlinedObject(go);
			outlinedBehaviour.OutlineRenderer = this;

			// behaviour will add itself using the OutlinedObject overload in it's OnEnable().
		}

		/// <summary>
		/// Add an object to outline to this <see cref="OutlineRenderer"/>
		/// </summary>
		public void AddOutlinedObject(Transform t)
		{
			AddOutlinedObject(t.gameObject);
		}


		internal void RemoveOutlinedObject(OutlinedObject obj)
		{
			if (outlinedObjects.Remove(obj))
				CreateCommandBuffer();
		}


		/// <summary>
		/// Remove an object to outline from this <see cref="OutlineRenderer"/>
		/// </summary>
		public void RemoveOutlinedObject(GameObject go)
		{
			var outlinedBehaviour = GetLinkedOutlinedObject(go);

			if (null == outlinedBehaviour)
			{
				if (null != go.GetComponent<OutlinedObject>())
				{
					Debug.LogWarning
					(
						$"Attempt to remove a GameObject from {nameof(OutlineRenderer)} that didn't have us as the OutlineCamera."
					  + $"Maybe it should have been removed from a different {nameof(OutlineRenderer)}?"
					);
				}
				else
				{
					Debug.LogWarning
					(
						$"Attempt to remove a GameObject from {nameof(OutlineRenderer)} that didn't have a {nameof(OutlinedObject)} component on it."
					);
				}

				return;
			}

			Destroy(outlinedBehaviour);
			// the behaviour will remove itself in it's OnDisable().
		}

		/// <summary>
		/// Remove an object to outline from this <see cref="OutlineRenderer"/>
		/// </summary>
		public void RemoveOutlinedObject(Transform t)
		{
			RemoveOutlinedObject(t.gameObject);
		}


		private void Clear_Internal()
		{
			foreach (var o in outlinedObjects.ToArray())
			{
				Destroy(o);
			}
			outlinedObjects.Clear();
		}

		/// <summary>
		/// Remove all <see cref="OutlinedObject"/>s
		/// </summary>
		public void ClearAllOutlinedObjects()
		{
			Clear_Internal();
			CreateCommandBuffer();
		}

		#endregion Targets

		/// <summary>
		/// Gets the <see cref="OutlinedObject"/> linked to this <see cref="OutlineRenderer"/>, or null
		/// </summary>
		/// <param name="go"></param>
		/// <returns></returns>
		private OutlinedObject GetLinkedOutlinedObject(GameObject go)
		{
			foreach (var component in go.GetComponents<OutlinedObject>())
			{
				if (component.OutlineRenderer == this)
				{
					return component;
				}
			}

			return null;
		}

		/// <summary>
		/// Gets the <see cref="OutlinedObject"/> linked to this <see cref="OutlineRenderer"/>, or creates a new one.
		/// </summary>
		/// <param name="go"></param>
		/// <returns></returns>
		private OutlinedObject GetLinkedOrAddNewOutlinedObject(GameObject go)
		{
			var linked = GetLinkedOutlinedObject(go);

			if (null != linked)
				return linked;
			else
				return go.AddComponent<OutlinedObject>();
		}


		/// <summary>
		/// (Re-)Create the command buffer that draws the outlines.
		/// </summary>
		public void CreateCommandBuffer()
		{
			// nothing to outline? cleanup
			if (outlinedObjects.Count == 0)
			{
				Cleanup();
				return;
			}

			// camera
			if (cam == null)
			{
				cam = GetComponent<Camera>();
				cam.depthTextureMode = DepthTextureMode.Depth;
			}

			if (cam.allowMSAA && ! hasComplainedAboutMSAA)
			{
				// todo: just make it work with MSAA

				hasComplainedAboutMSAA = true; // Don't spam the log
				Debug.LogError($"Outline will not render properly with MSAA enabled.");
			}

			if (! cam.allowMSAA)
			{
				// Reset the warning, so that it triggers again when re-enabled later.
				hasComplainedAboutMSAA = false;
			}

			// material
			if (outlineMaterial == null)
			{
				outlineMaterial = new Material(Shader.Find("Hidden/Outliner"));
			}

			// command buffer
			if (commandBuffer == null)
			{
				commandBuffer = new CommandBuffer();
				commandBuffer.name = commandBufferName;
				cam.AddCommandBuffer(bufferDrawEvent, commandBuffer);
			}
			else
			{
				commandBuffer.Clear();
			}

			// initialization
			int width = (cam.targetTexture  != null) ? cam.targetTexture.width : cam.pixelWidth;
			int height = (cam.targetTexture != null) ? cam.targetTexture.height : cam.pixelHeight;
			int aTempID = Shader.PropertyToID("_aTemp");
			commandBuffer.GetTemporaryRT(aTempID, width, height, 0, FilterMode.Bilinear, RenderTextureFormat.ARGB32);
			commandBuffer.SetRenderTarget(aTempID, BuiltinRenderTextureType.CurrentActive);
			commandBuffer.ClearRenderTarget(false, true, Color.clear);

			// render selected objects into a mask buffer, with different colors for visible vs occluded ones
			float id = 0f;
			foreach (var outlinedObject in outlinedObjects)
			{
				id += 0.25f;
				commandBuffer.SetGlobalFloat("_ObjectId", id);

				foreach (var render in outlinedObject.Renderers)
				{
					Material mat = outlineMaterial;
					if (render.material.HasTexture("_MainTex") // getter for mainTexture complains to log if it's not defined.
						&& render.material.mainTexture != null)
					{
						mat = new Material(outlineMaterial);
						mat.SetTexture("_MainTex", render.material.mainTexture);
						mat.SetFloat("_Cutoff", alphaCutoff);
						mat.SetFloat("_DoClip", 1);
					}

					for (int i = 0; i < render.materials.Length; i++)
					{
						// We make the assumption here that the renderer will have a material for each subMesh.
						// What could possibly go wrong?
						commandBuffer.DrawRenderer(render, mat, i, 1);
						commandBuffer.DrawRenderer(render, mat, i, 0);
					}
				}
			}


			// object ID edge detection pass
			int bTempID = Shader.PropertyToID("_bTemp");
			commandBuffer.GetTemporaryRT(bTempID, width, height, 0, FilterMode.Bilinear, RenderTextureFormat.ARGB32);
			commandBuffer.Blit(aTempID, bTempID, outlineMaterial, 3);

			// Blur - adjusting blur size to appear the same size, no matter the resolution
			float proportionalBlurSize = ((float) height / 1080f) * blurSize;
			int cTempID = Shader.PropertyToID("_cTemp");
			commandBuffer.GetTemporaryRT(cTempID, width, height, 0, FilterMode.Bilinear, RenderTextureFormat.ARGB32);
			commandBuffer.SetGlobalVector("_BlurDirection", new Vector2(proportionalBlurSize, 0));
			commandBuffer.Blit(bTempID, cTempID, outlineMaterial, 2);
			commandBuffer.SetGlobalVector("_BlurDirection", new Vector2(0, proportionalBlurSize));
			commandBuffer.Blit(cTempID, bTempID, outlineMaterial, 2);

			// final overlay
			commandBuffer.SetGlobalColor("_OutlineColor", outlineColor);
			commandBuffer.SetGlobalFloat("_OutlineInFrontOpacity", occludedOutlineOpacity);
			commandBuffer.Blit(bTempID, BuiltinRenderTextureType.CameraTarget, outlineMaterial, 4);

			// release tempRTs
			commandBuffer.ReleaseTemporaryRT(aTempID);
			commandBuffer.ReleaseTemporaryRT(bTempID);
			commandBuffer.ReleaseTemporaryRT(cTempID);
		}
	}
}