﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


// This file is a modification of the original, which has the following copyright:
// Copyright (c) 2018 Michael Curtiss
// The Original, licensed MIT can be found here: https://github.com/brettmjohnson/Outliner



using UnityEngine;

namespace OutlineFx
{
	public class OutlinedObject : MonoBehaviour
	{
		/// <summary>
		/// Has Start() been called
		/// </summary>
		private bool started;

		public Renderer[] Renderers { get; protected set; }


		[SerializeField]
		private OutlineRenderer _outlineRenderer;
		public OutlineRenderer OutlineRenderer
		{
			get => _outlineRenderer;
			set
			{
				if (_outlineRenderer == value) return;
				if (! started)
				{
					_outlineRenderer = value;
					return;
				}

				// Has started
				_outlineRenderer?.RemoveOutlinedObject(this);
				_outlineRenderer = value;
				_outlineRenderer.AddOutlinedObject(this);
			}
		}



		private void OnEnable()
		{
			if(started) OnEnableAfterStart();
		}

		private void Start()
		{
			Renderers = GetComponentsInChildren<Renderer>();

			if (null == OutlineRenderer) OutlineRenderer = FindObjectOfType<OutlineRenderer>();

			started = true;
			OnEnableAfterStart();
		}

		private void OnEnableAfterStart()
		{
			if (null == OutlineRenderer) return;

			OutlineRenderer.AddOutlinedObject(this);
		}


		private void OnDisable()
		{
			if (null == OutlineRenderer) return;

			OutlineRenderer.RemoveOutlinedObject(this);
		}

	}
}